﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MemoApp.InternoHelper
{
    public class InternoDB
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Tipo;
        public string Nombre { get; set; }
        public string Numero { get; set; }
    }
}
