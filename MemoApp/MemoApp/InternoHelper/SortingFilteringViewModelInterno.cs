﻿using MemoApp.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms.Internals;

namespace MemoApp.InternoHelper
{
    [Preserve(AllMembers = true)]
    public class SortingFilteringDosisViewModelInterno : INotifyPropertyChanged
    {
        #region Fields

        private ListViewSortOptions sortingOptions;

        #endregion

        #region Constructor
        public SortingFilteringDosisViewModelInterno()
        {
            AddItemDetails();
        }

        #endregion

        #region Properties

        public ObservableCollection<InternoModel> Items
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value whether indicates the sorting options, ascending or descending or none.
        /// </summary>
        public ListViewSortOptions SortingOptions
        {
            get
            {
                return sortingOptions;
            }
            set
            {
                sortingOptions = value;
                OnPropertyChanged("SortingOptions");
            }
        }

        #endregion

        #region Methods
        private void AddItemDetails()
        {
            Items = new ObservableCollection<InternoModel>();
            //var random = new Random();

            var lDbHelp = new DBHelper();
            var listado = lDbHelp.ObtenerInternosDB();

            foreach (var lInternoBase in listado.GroupBy(g => g.Tipo).OrderBy(o => o.Key).Select(s => s.Key))
            {
                var linterno = new InternoModel();

                if (lInternoBase != "")
                {
                    linterno.Nombre = lInternoBase;
                    var contador = 0;

                    foreach (var item in listado.Where(x=>x.Tipo == lInternoBase))
                    {
                        if (contador == 0)
                        {
                            linterno.Descripcion = item.Nombre + ": " + item.Numero;
                        }
                        else
                        {
                            linterno.Descripcion = linterno.Descripcion + "\n" + item.Nombre + ": " + item.Numero;

                        }

                        contador++;
                    }
                    Items.Add(linterno);

                }

            }

        }





        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }

    public enum ListViewSortOptions
    {
        None,
        Ascending,
        Descending
    }
}
