﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms.Internals;
namespace MemoApp.InternoHelper
{
    [Preserve(AllMembers = true)]
    public class InternoModel : INotifyPropertyChanged
    {
        #region Fields

        private string _Id;
        private string _Tipo;
        private string _Nombre;
        private string _Numero;
        private string _Descripcion;


        #endregion
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value;
                this.RaisePropertyChanged("Nombre");
            }
        }

        public string Numero
        {
            get
            {
                return _Descripcion;
            }
            set
            {
                _Descripcion = value;
                this.RaisePropertyChanged("Descripcion");
            }
        }

        public string Tipo
        {
            get
            {
                return _Tipo;
            }
            set
            {
                _Tipo = value;
                this.RaisePropertyChanged("Tipo");
            }
        }

        public string Descripcion
        {
            get
            {
                return _Descripcion;
            }
            set
            {
                _Descripcion = value;
                this.RaisePropertyChanged("Descripcion");
            }

        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(String name)
        {
            if (PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
