﻿using Syncfusion.SfPicker.XForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace MemoApp.CalculadoraHelper
{
    public class Picker : SfPicker
    {
        #region Public Properties



        /// <summary>
        /// Date is the acutal DataSource for SfPicker control which will holds the collection of Day ,Month and Year
        /// </summary>
        /// <value>The date.</value>
        public ObservableCollection<string> Opciones { get; set; }




        /// <summary>
        /// Headers api is holds the column name for every column in date picker
        /// </summary>
        /// <value>The Headers.</value>
        public ObservableCollection<string> Headers { get; set; }

        #endregion

        public Picker()
        {
            Opciones = new ObservableCollection<string>();
            Headers = new ObservableCollection<string>();
            HeaderText = "¿Que conversion queres hacer?";
            PopulateDateCollection();
            this.ItemsSource = Opciones;
            this.ColumnHeaderText = Headers;
            ShowFooter = true;
            ShowHeader = true;
            ShowColumnHeader = true;
        }



        private void PopulateDateCollection()
        {
            Opciones.Add("");
            Opciones.Add("Ml/h a Mcg/kg/min");
            Opciones.Add("Mcg/kg/min a Ml/h");
        }
    }


}
