﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using Syncfusion.SfPicker.XForms;
using Xamarin.Forms;

namespace MemoApp.CalculadoraHelper
{
    public class CalculadoraViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<object> _opcion;

        public ObservableCollection<object> Opcion
        {
            get { return _opcion; }
            set { _opcion = value; RaisePropertyChanged("Opcion"); }
        }

        void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public CalculadoraViewModel()
        {
            ObservableCollection<object> opcionesCollection = new ObservableCollection<object>();

            
            this.Opcion = opcionesCollection;
        }
    }
}
