﻿using MemoApp.Models;
using MemoApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Syncfusion.ListView.XForms;
using Syncfusion.SfNavigationDrawer.XForms;
using System.Collections.ObjectModel;
using MemoApp.Helpers;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Extensions;


namespace MemoApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            hamburgerButton.Image = (FileImageSource)ImageSource.FromFile("hamburgericon.png");
            AppIcon.Source = ImageSource.FromFile("anestesia.png");

            List<MenuCollectionModel> listMenu = new List<MenuCollectionModel>();


            listMenu.Add(new MenuCollectionModel() { MenuItem = "Datos Paciente", Icon = "R", FontColor = Color.Black });
            listMenu.Add(new MenuCollectionModel() { MenuItem = "Param. Vitales", Icon = "D" });
            listMenu.Add(new MenuCollectionModel() { MenuItem = "Via Aerea", Icon = "O" });
            listMenu.Add(new MenuCollectionModel() { MenuItem = "Dosis", Icon = "H" });
            listMenu.Add(new MenuCollectionModel() { MenuItem = "Reversion Muscular", Icon = "Q" });
            listMenu.Add(new MenuCollectionModel() { MenuItem = "Internos", Icon = "B" });
            listMenu.Add(new MenuCollectionModel() { MenuItem = "Calculadora", Icon = "Z" });


            listView.ItemsSource = listMenu;
        }

        void hamburgerButton_Clicked(object sender, EventArgs e)
        {
            navigationDrawer.ToggleDrawer();
        }

        async void Handle_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            var tempListView = sender as SfListView;
            for (int i = 0; i < 5; i++)
            {
                var tempItem = (listView.ItemsSource as List<MenuCollectionModel>)[i];
                if ((e.ItemData as MenuCollectionModel) != tempItem)
                {
                    tempItem.FontColor = Color.FromHex("#8e8e92");
                }
            }

            var temp = e.ItemData as MenuCollectionModel;
            temp.FontColor = Color.FromHex("#007ad0");

            if ((e.ItemData as MenuCollectionModel).MenuItem == "Datos Paciente")
            {
                try
                {

                    navigationDrawer.ContentView = new DatosPacientes().Content;


                }
                catch (Exception ex)
                {

                    throw;
                }
            }


            if ((e.ItemData as MenuCollectionModel).MenuItem == "Param. Vitales")
            {
                try
                {
                    navigationDrawer.ContentView = new SignosVitales().Content;

                    if (Settings.Paciente != null)
                    {
                        navigationDrawer.ContentView = new SignosVitales().Content;
                    }
                    else
                    {
                        //Instancio el popup por si lo voy a usar
                        var MensajeShow = new PopUp("Para acceder al menu, debe cargar los datos del paciente");

                        MensajeShow.Animation = new ScaleAnimation
                        {
                            DurationOut = 300,
                            DurationIn = 300,
                            EasingIn = Easing.SinIn,
                            EasingOut = Easing.SpringIn
                        };

                        MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                        MensajeShow.CloseWhenBackgroundIsClicked = true;

                        await Navigation.PushPopupAsync(MensajeShow);
                    }

                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            if ((e.ItemData as MenuCollectionModel).MenuItem == "Dosis")
            {
                try
                {
                    navigationDrawer.ContentView = new Dosis().Content;
                    /*
                    if (Settings.Paciente != null)
                    {
                        navigationDrawer.ContentView = new Dosis().Content;
                    }
                    else
                    {
                        //Instancio el popup por si lo voy a usar
                        var MensajeShow = new PopUp("Para acceder al menu, debes cargar los datos del paciente");

                        MensajeShow.Animation = new ScaleAnimation
                        {
                            DurationOut = 300,
                            DurationIn = 300,
                            EasingIn = Easing.SinIn,
                            EasingOut = Easing.SpringIn
                        };

                        MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                        MensajeShow.CloseWhenBackgroundIsClicked = true;

                        await Navigation.PushPopupAsync(MensajeShow);
                    }*/

                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            if ((e.ItemData as MenuCollectionModel).MenuItem == "Reversion Muscular")
            {
                try
                {

                    if (Settings.Paciente != null)
                    {
                        navigationDrawer.ContentView = new ReversionMuscular().Content;
                    }
                    else
                    {
                        //Instancio el popup por si lo voy a usar
                        var MensajeShow = new PopUp("Para acceder al menu, debe cargar los datos del paciente");

                        MensajeShow.Animation = new ScaleAnimation
                        {
                            DurationOut = 300,
                            DurationIn = 300,
                            EasingIn = Easing.SinIn,
                            EasingOut = Easing.SpringIn
                        };

                        MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                        MensajeShow.CloseWhenBackgroundIsClicked = true;

                        await Navigation.PushPopupAsync(MensajeShow);
                    }


                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            if ((e.ItemData as MenuCollectionModel).MenuItem == "Internos")
            {
                try
                {

                    navigationDrawer.ContentView = new Internos().Content;


                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            if ((e.ItemData as MenuCollectionModel).MenuItem == "Calculadora")
            {
                try
                {

                    navigationDrawer.ContentView = new Calculadora().Content;


                }
                catch (Exception ex)
                {

                    throw;
                }
            }


            //navigationDrawer.ContentView = new Archive_Default(temp.MessageContent, (e.ItemData as MenuCollectionModel).MenuItem).Content;
            if (Device.OS == TargetPlatform.iOS)
                navigationDrawer.IsOpen = false;
            else
                navigationDrawer.ToggleDrawer();



        }

    }
}
