﻿using Syncfusion.XForms.DataForm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms.Internals;

namespace MemoApp
{
    public class PacienteModel : INotifyPropertyChanged
    {


        private int peso;

        public int Peso
        {
            get { return peso; }
            set { peso = value; OnPropertyChanged(); }
        }

        private int edad;

        public int Edad
        {
            get { return edad; }
            set { edad = value; OnPropertyChanged(); }
        }

        private int altura;

        public int Altura
        {
            get { return altura; }
            set { altura = value; OnPropertyChanged(); }
        }

        private int asa;

        public int ASA
        {
            get { return asa; }
            set { asa = value; OnPropertyChanged(); }
        }



        private string sexo;

        public string Sexo
        {
            get { return sexo; }
            set { sexo = value; OnPropertyChanged(); }
        }

        private bool isbusy = false;


        public bool IsBusy
        {
            get { return isbusy; }
            set { isbusy = value; OnPropertyChanged(); }
        }


        public double PesoIdeal
        {
            get
            {

                if (BMI > 25)
                {
                    //Estimacion de Devine Modificada
                    var resultado = 0.00;


                    if (sexo == "Hombre")
                    {
                        resultado = 50 + ((Convert.ToDouble(altura) - 150) * 0.921);
                    }

                    if (sexo == "Mujer")
                    {
                        resultado = 45.5 + ((Convert.ToDouble(altura) - 150) * 0.921);

                    }

                    return Math.Round(resultado, 0);
                }
                else
                {
                    return peso;
                }

            }
        }


        public double PesoCorregido
        {
            get
            {
                if (BMI > 25)
                {
                    return
                  Math.Round(PesoIdeal + 0.4 * (Convert.ToDouble(peso) - PesoIdeal), 0);
                }
                else
                {
                    return peso;
                }
            }
        }


        public double PesoMagro
        {
            get
            {
                if (BMI > 25)
                {
                    var resultado = 0.00;


                    if (sexo == "Hombre")
                    {
                        resultado = (9270 * Convert.ToDouble(peso)) / (6680 + 216 * BMI);
                    }

                    if (sexo == "Mujer")
                    {
                        resultado = (9270 * Convert.ToDouble(peso)) / (8780 + 244 * BMI);

                    }

                    return Math.Round(resultado, 0);
                }
                else
                {
                    return peso;
                }
            }
        }


        public double BMI
        {
            get
            {
                double lAlturaMetro = (double)altura / 100;
                double lAlturaCuadrada =lAlturaMetro * lAlturaMetro;
                var lResultado = (double)peso / lAlturaCuadrada;
                return Math.Round(lResultado, 0);
            }
        }






        protected bool SetProperty<T>(
           ref T backingStore, T value,
           [CallerMemberName]string propertyName = "",
           Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;


        protected void OnPropertyChanged([CallerMemberName]string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

    }
}
