﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace MemoApp.Models
{
    public class ReversionModel : INotifyPropertyChanged
    {


        private string _TipoControl;

        public string TipoControl
        {
            get { return _TipoControl; }
            set { _TipoControl = value; }
        }

        private string _Relajante;

        public string Relajante
        {
            get { return _Relajante; }
            set { _Relajante = value; }
        }

        private string _RespuestaTOF;

        public string RespuestaTOF
        {
            get { return _RespuestaTOF; }
            set { _RespuestaTOF = value; }
        }


        private string _RespuestaPTC;

        public string RespuestaPTC
        {
            get { return _RespuestaPTC; }
            set { _RespuestaPTC = value; }
        }



        protected bool SetProperty<T>(
          ref T backingStore, T value,
          [CallerMemberName]string propertyName = "",
          Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;


        protected void OnPropertyChanged([CallerMemberName]string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
