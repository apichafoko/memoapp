﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace MemoApp.Models
{
    public class MenuCollectionModel : INotifyPropertyChanged
    {
        
        private string menuItem;
        public MenuCollectionModel()
        {
        }

        public string MenuItem
        {
            get
            {
                return menuItem;
            }

            set
            {
                menuItem = value;
            }
        }

        public string Icon
        {
            get;
            set;
        }
        private Color fontColor = Color.FromHex("#8e8e92");
        public Color FontColor
        {
            get

            {
                return fontColor;
            }
            set
            {
                fontColor = value;
                OnPropertyChanged("FontColor");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
