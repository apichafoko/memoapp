﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace MemoApp.Models
{
    public class SexoInfo

    {

        private ObservableCollection<string> _sexo;

        public ObservableCollection<string> Sexos

        {

            get { return _sexo; }

            set { _sexo = value; }

        }

        public SexoInfo()

        {

            Sexos = new ObservableCollection<string>();

            Sexos.Add("Hombre");

            Sexos.Add("Mujer");

          

        }

    }
}
