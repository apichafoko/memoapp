﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Syncfusion.ListView.XForms;
using Xamarin.Forms.Internals;

namespace MemoApp.SignosHelpers
{
    public class SignosVitalesViewModel
    {
        #region Fields

        private ObservableCollection<SignosVitalesModel> _SignosVitalesCollection;

        #endregion

        #region Constructor

        public SignosVitalesViewModel()
        {
            GenerateSource();
        }

        #endregion

        #region Properties

        public ObservableCollection<SignosVitalesModel> SignosVitalesCollection
        {
            get { return _SignosVitalesCollection; }
            set { this._SignosVitalesCollection = value; }
        }

        #endregion

        #region Generate Source

        private void GenerateSource()
        {
            SignosVitalesRepository SignosRepository = new SignosVitalesRepository();
            foreach (var item in SignosRepository.GetSignos())
            {
               
            }
            //SignosVitalesCollection = SignosRepository.GetSignos();
        }

        #endregion
    }
}
