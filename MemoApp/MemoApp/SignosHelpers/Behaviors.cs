﻿using Syncfusion.DataSource;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace MemoApp.SignosHelpers
{
    public class SignosVitalesBehavior : Behavior<Syncfusion.ListView.XForms.SfListView>
    {
        #region Fields

        private SignosVitalesViewModel autofitViewModel;
        private Syncfusion.ListView.XForms.SfListView ListView;

        #endregion

        #region Overrides
        protected override void OnAttachedTo(Syncfusion.ListView.XForms.SfListView bindable)
        {
            ListView = bindable;
            autofitViewModel = new SignosVitalesViewModel();
            ListView.BindingContext = autofitViewModel;
            ListView.ItemsSource = autofitViewModel.SignosVitalesCollection;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Syncfusion.ListView.XForms.SfListView bindable)
        {
            base.OnDetachingFrom(bindable);
        }

        #endregion

    }

}
