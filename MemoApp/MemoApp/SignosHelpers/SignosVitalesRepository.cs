﻿using MemoApp.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace MemoApp.SignosHelpers
{
    public class SignosVitalesRepository
    {
        #region Constructor

        public SignosVitalesRepository()
        {

        }

        #endregion

        #region Properties

        internal ObservableCollection<SignosVitalesModel> GetSignos()
        {
            var SignosCollect = new ObservableCollection<SignosVitalesModel>();

            foreach (var item in GetCategorias())
            {
                //TODO: Cambiar a que obtenga de la BD segun la categoria
                if (item == "Cardiaco")
                {
                    SignosCollect.Add(new SignosVitalesModel() {Categoria = item, Nombre = "Frec. Cardiaca", Descripcion = "70 - 100" });
                    SignosCollect.Add(new SignosVitalesModel() { Categoria = item, Nombre = "Presión  Arterial", Descripcion = "120 - 65" });

                }

                if (item == "Respiratorio")
                {
                    SignosCollect.Add(new SignosVitalesModel() { Categoria = item, Nombre = "Frec. Resp.", Descripcion = "14 - 18" });
                    SignosCollect.Add(new SignosVitalesModel() { Categoria = item, Nombre = "Vol. Tidal", Descripcion = "420 - 560 ml" });
                    SignosCollect.Add(new SignosVitalesModel() { Categoria = item, Nombre = "Vol. Minuto", Descripcion = "4.5 - 7 L/min" });

                }


                if (item == "Varios")
                {
                    SignosCollect.Add(new SignosVitalesModel() { Categoria = item, Nombre = "Peso Real", Descripcion = Settings.Paciente.Peso.ToString() + " kg" });
                    SignosCollect.Add(new SignosVitalesModel() { Categoria = item, Nombre = "Peso Corregido", Descripcion = Settings.Paciente.PesoCorregido.ToString() + " kg" });
                    SignosCollect.Add(new SignosVitalesModel() { Categoria = item, Nombre = "Peso Ideal", Descripcion = Settings.Paciente.PesoIdeal.ToString() + " kg" });
                    SignosCollect.Add(new SignosVitalesModel() { Categoria = item, Nombre = "Peso Magro", Descripcion = Settings.Paciente.PesoMagro.ToString() + " kg" });

                }
            }           



            return SignosCollect;
        }

        public List<string> GetCategorias()
        {
            var lListadoReturn = new List<string>();
            lListadoReturn.Add("Cardiaco");
            lListadoReturn.Add("Respiratorio");
            lListadoReturn.Add("Varios");

            return lListadoReturn;
        }

        #endregion

        #region BookInfo

        

        #endregion
    }

}
