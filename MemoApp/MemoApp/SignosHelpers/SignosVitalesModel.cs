﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace MemoApp.SignosHelpers
{
    [Preserve(AllMembers = true)]
    public class SignosVitalesModel : INotifyPropertyChanged
    {
        #region Fields

        private string _Categoria;
        private string _Nombre;
        private string _Descripcion;

        #endregion

        #region Constructor

        public SignosVitalesModel()
        {

        }

        #endregion

        #region Properties

        public string Nombre
        {
            get { return _Nombre; }
            set
            {
                _Nombre = value;
                OnPropertyChanged("Nombre");
            }
        }


        public string Categoria
        {
            get { return _Categoria; }
            set
            {
                _Categoria = value;
                OnPropertyChanged("Categoria");
            }
        }

        public string Descripcion
        {
            get { return _Descripcion; }
            set
            {
                _Descripcion = value;
                OnPropertyChanged("Descripcion");
            }
        }

       

        #endregion

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
