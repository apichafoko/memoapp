﻿using MemoApp.DosisHelpers;
using MemoApp.InternoHelper;
using System;
using System.Collections.Generic;
using System.Text;


namespace MemoApp.Helpers
{
    public class DBHelper
    {
        public List<FarmacoBase> ObtenerFarmacosBase()
        {
            var JsonHelp = new JsonHelper();
            return JsonHelp.ObtenerFarmacosBase();
        }

        public List<FarmacoDB> ObtenerFarmacos(string aASA, string aFarmaco)
        {
            var JsonHelp = new JsonHelper();
            return JsonHelp.ObtenerFarmacosByASA(aASA, aFarmaco);
        }

        public List<InternoDB> ObtenerInternosDB()
        {
            var JsonHelp = new JsonHelper();
            return JsonHelp.ObtenerInternosDB();
        }
    }
}
