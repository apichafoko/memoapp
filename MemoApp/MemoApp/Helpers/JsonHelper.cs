﻿using MemoApp.DosisHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Linq;
using MemoApp.InternoHelper;

namespace MemoApp.Helpers
{
    public class JsonHelper
    {
        public List<FarmacoDB> ObtenerFarmacosByASA(string aASA, string aFarmaco)
        {
            #region How to load an Json file embedded resource
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(JsonHelper)).Assembly;

            Stream stream = assembly.GetManifestResourceStream("MemoApp.FarmacosDB.json");


            using (var reader = new System.IO.StreamReader(stream))
            {

                var json = reader.ReadToEnd();
                
                //TODO: FALTA AGREGAR EL TIPO (INDUCTOR, ANALGESICO, ANTIHEMETICO, ETC).
                return JsonConvert.DeserializeObject<List<FarmacoDB>>(json).Where(x=> x.ASA.Contains(aASA) && x.Nombre == aFarmaco).ToList();         
                                
            }
            #endregion
          
        }

        public List<FarmacoBase> ObtenerFarmacosBase()
        {
            #region How to load an Json file embedded resource
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(JsonHelper)).Assembly;

            Stream stream = assembly.GetManifestResourceStream("MemoApp.FarmacosBase.json");

            var lListadoReturn = new List<FarmacoBase>();


            using (var reader = new System.IO.StreamReader(stream))
            {

                var json = reader.ReadToEnd();

                lListadoReturn = JsonConvert.DeserializeObject<List<FarmacoBase>>(json);

            }
            #endregion

            return lListadoReturn;

            // NOTE: use for debugging, not in released app code!
            //foreach (var res in assembly.GetManifestResourceNames()) 
            //	System.Diagnostics.Debug.WriteLine("found resource: " + res);
        }

        public List<InternoDB> ObtenerInternosDB()
        {
            #region How to load an Json file embedded resource
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(JsonHelper)).Assembly;

            Stream stream = assembly.GetManifestResourceStream("MemoApp.Internos.json");

            var lListadoReturn = new List<InternoDB>();


            using (var reader = new System.IO.StreamReader(stream))
            {

                var json = reader.ReadToEnd();

                lListadoReturn = JsonConvert.DeserializeObject<List<InternoDB>>(json);

            }
            #endregion

            return lListadoReturn;

            // NOTE: use for debugging, not in released app code!
            //foreach (var res in assembly.GetManifestResourceNames()) 
            //	System.Diagnostics.Debug.WriteLine("found resource: " + res);
        }

    }
}
