﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MemoApp.Helpers
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
