﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms.Internals;

namespace MemoApp.DosisHelpers
{
    [Preserve(AllMembers = true)]
    public class FarmacoViewModel : INotifyPropertyChanged
    {
        #region Fields

        private string _Nombre;
        private string _Descripcion;
        private string _Tipo;

        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value;
                this.RaisePropertyChanged("Nombre");
            }
        }

        public string Descripcion
        {
            get
            {
                return _Descripcion;
            }
            set
            {
                _Descripcion = value;
                this.RaisePropertyChanged("Descripcion");
            }
        }

        public string Tipo
        {
            get
            {
                return _Tipo;
            }
            set
            {
                _Tipo = value;
                this.RaisePropertyChanged("Tipo");
            }
        }


        public string TipoAdmin { get; set; }
        public double DosisDesde { get; set; }
        public double DosisHasta { get; set; }
        //Mg/Mcg/Mg
        public string Medida { get; set; }
        public string DosisCompleta { get; set; }
        public string Fuente { get; set; }
        public string PDF { get; set; }
        public string ASA { get; set; }

        #endregion


        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(String name)
        {
            if (PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
