﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MemoApp.DosisHelpers
{
    public class FarmacoBase
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Nombre;
        public string Tipo { get; set; }        
    }
}
