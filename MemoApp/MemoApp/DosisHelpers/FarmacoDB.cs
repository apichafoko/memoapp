﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MemoApp.DosisHelpers
{
    public class FarmacoDB
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Nombre;
        public string TipoAdmin { get; set; }
        public double DosisDesde { get; set; }
        public double DosisHasta { get; set; }
        public double EdadDesde { get; set; }
        public double EdadHasta { get; set; }
        //Mg/Mcg/Mg
        public string Medida { get; set; }
        public string Fuente { get; set; }
        public string ASA { get; set; }
    }
}
