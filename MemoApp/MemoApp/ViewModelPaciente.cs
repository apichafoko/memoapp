﻿using MemoApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MemoApp
{
    public class ViewModelPaciente
    {
        private PacienteModel pacienteModel;
        public PacienteModel PacienteModel
        {
            get { return this.pacienteModel; }
            set { this.pacienteModel = value; }
        }
        public ViewModelPaciente()
        {
            this.pacienteModel = new PacienteModel();
        }
    }
}
