﻿using MemoApp.Helpers;
using MemoApp.Models;
using Syncfusion.SfNavigationDrawer.XForms;
using Syncfusion.SfPicker.XForms;
using Syncfusion.XForms.DataForm;
using Syncfusion.XForms.PopupLayout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DatosPacientes : ContentPage
    {
        SfPopupLayout popupLayout;
        public DatosPacientes()
        {

            InitializeComponent();
            popupLayout = new SfPopupLayout();
            SexoInfo info = new SexoInfo();


            headerLabel.Text = "Datos Paciente";
            btn.BackgroundColor = Color.FromHex("#00a0e1");

            btn.HeightRequest = 40;
            btn.WidthRequest = 40;
            btn.Text = "H";
            btn.FontSize = 16;

            if (Device.OS == TargetPlatform.Android)
            {
                if (Device.Idiom == TargetIdiom.Tablet)
                    btn.WidthRequest = 60;
                btn.FontFamily = "navigation.ttf#navigation";
            }
            else
            {
                btn.Image = (FileImageSource)ImageSource.FromFile("hamburgericon.png");
                btn.FontFamily = "navigation";
            }
            btn.TextColor = Color.White;


            if (Settings.Paciente != null)
            {
                PesoEntry.Value = Settings.Paciente.Peso.ToString();
                PesoEntry.Watermark = Settings.Paciente.Peso.ToString();

                AlturaEntry.Value = Settings.Paciente.Altura.ToString();
                AlturaEntry.Watermark = Settings.Paciente.Altura.ToString();

                EdadEntry.Value = Settings.Paciente.Edad.ToString();
                EdadEntry.Watermark = Settings.Paciente.Edad.ToString();

                if (Settings.Paciente != null)
                {
                    ASA.SelectedItem = Settings.Paciente.ASA;
                    ASA.Title = Settings.Paciente.ASA.ToString();
                }

                if (Settings.Paciente.Sexo != null)
                {
                    Sexo.SelectedItem = Settings.Paciente.Sexo;
                    Sexo.Title = Settings.Paciente.Sexo;
                }
            }

        }



        void Btn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IToggleDrawer>().ToggleDrawer();
        }


        async void BtnAceptar_Clicked(object sender, EventArgs e)
        {
            var Errores = 0;

            #region ControlVacio
            if (PesoEntry.Value == null)
            {
                lblErrorPeso.Text = "El peso es obligatorio";
                lblErrorPeso.TextColor = Color.Red;
                Errores++;
            }

            if (AlturaEntry.Value == null)
            {
                lblErrorAltura.Text = "El peso es obligatorio";
                lblErrorAltura.TextColor = Color.Red;
                Errores++;
            }
            if (EdadEntry.Value == null)
            {
                lblErrorEdad.Text = "El peso es obligatorio";
                lblErrorEdad.TextColor = Color.Red;
                Errores++;
            }

            if (Sexo.SelectedItem == null)
            {
                lblErrorSexo.Text = "El sexo es obligatorio";
                lblErrorSexo.TextColor = Color.Red;
                Errores++;
            }

            if (ASA.SelectedItem == null)
            {
                lblErrorASA.Text = "El ASA es obligatorio";
                lblErrorASA.TextColor = Color.Red;
                Errores++;
            }

            #endregion
            #region Control
            if (Errores == 0)
            {
                if (Convert.ToInt32(PesoEntry.Value) > 200)
                {
                    lblErrorPeso.Text = "El valor del peso es mayor al limite permitido";
                    lblErrorPeso.TextColor = Color.Red;
                    lblErrorPeso.FontAttributes = FontAttributes.Italic;
                    lblErrorPeso.FontSize = 12;
                    Errores++;
                }
                else
                {
                    lblErrorPeso.Text = "";
                }

                if (Convert.ToInt32(AlturaEntry.Value) > 220)
                {
                    lblErrorAltura.Text = "El valor de la altura es mayor al limite permitido";
                    lblErrorAltura.TextColor = Color.Red;
                    lblErrorAltura.FontAttributes = FontAttributes.Italic;
                    lblErrorAltura.FontSize = 12;
                    Errores++;
                }
                else
                {
                    lblErrorAltura.Text = "";
                }

                if (Convert.ToInt32(EdadEntry.Value) > 100)
                {
                    lblErrorEdad.Text = "El valor de la edad es mayor al limite permitido";
                    lblErrorEdad.TextColor = Color.Red;
                    lblErrorEdad.FontAttributes = FontAttributes.Italic;
                    lblErrorEdad.FontSize = 12;
                    Errores++;
                }
                else
                {
                    lblErrorEdad.Text = "";
                }


                if (Convert.ToInt32(AlturaEntry.Value) < 120)
                {
                    lblErrorEdad.Text = "El valor de la altura es incorrecto.";
                    lblErrorEdad.TextColor = Color.Red;
                    lblErrorEdad.FontAttributes = FontAttributes.Italic;
                    lblErrorEdad.FontSize = 12;
                    Errores++;
                }
                else
                {
                    lblErrorEdad.Text = "";
                }

                if (Convert.ToInt32(EdadEntry.Value) == 0)
                {
                    lblErrorEdad.Text = "El valor de la edad es incorrecto.";
                    lblErrorEdad.TextColor = Color.Red;
                    lblErrorEdad.FontAttributes = FontAttributes.Italic;
                    lblErrorEdad.FontSize = 12;
                    Errores++;
                }
                else
                {
                    lblErrorEdad.Text = "";
                }
            }
            #endregion



            //Solo adultos por el momento
            if (Convert.ToInt32(EdadEntry.Value) < 18 && Convert.ToInt32(EdadEntry.Value) > 0)
            {
                lblErrorEdad.Text = "Por el momento solo es válido para adultos";
                lblErrorEdad.TextColor = Color.Red;
                lblErrorEdad.FontAttributes = FontAttributes.Italic;
                lblErrorEdad.FontSize = 12;
                Errores++;
            }
            else
            {
                lblErrorEdad.Text = "";
            }

            if (Errores == 0)
            {
                Settings.Paciente = new PacienteModel()
                {
                    Peso = Convert.ToInt32(PesoEntry.Value),
                    Edad = Convert.ToInt32(EdadEntry.Value),
                    Altura = Convert.ToInt32(AlturaEntry.Value),
                    ASA = Convert.ToInt32(ASA.SelectedItem),
                    Sexo = Sexo.SelectedItem.ToString()
                };

                await Application.Current.MainPage.DisplayAlert("Datos Paciente", "Los datos han sido actualizados", "OK");





            }

        }
    }
}