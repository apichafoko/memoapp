﻿using MemoApp.CalculadoraHelper;
using MemoApp.Helpers;
using MemoApp.Models;
using Syncfusion.SfNavigationDrawer.XForms;
using Syncfusion.SfPicker.XForms;
//using Syncfusion.XForms.PopupLayout;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Calculadora : ContentPage, INotifyPropertyChanged
    {
        ObservableCollection<object> todaycollection = new ObservableCollection<object>();
        public int Opcion = -1;

        public Calculadora()
        {

            InitializeComponent();

            #region FooterPicker
            var lLayout = new StackLayout();
            var lBotonOk = new Button();
            lBotonOk.Text = "OK";
            lBotonOk.BackgroundColor = Color.White;
            lBotonOk.TextColor = Color.Black;
            lBotonOk.Clicked += lBotonOk_Clicked;
            lLayout.Children.Add(lBotonOk);
            opcionPicker.FooterView = lLayout;
            #endregion

            headerLabel.Text = "Calculadora";
            btn.BackgroundColor = Color.FromHex("#00a0e1");

            btn.HeightRequest = 40;
            btn.WidthRequest = 40;
            btn.Text = "H";
            btn.FontSize = 16;

            if (Device.OS == TargetPlatform.Android)
            {
                if (Device.Idiom == TargetIdiom.Tablet)
                    btn.WidthRequest = 60;
                btn.FontFamily = "navigation.ttf#navigation";
            }
            else
            {
                btn.Image = (FileImageSource)ImageSource.FromFile("hamburgericon.png");
                btn.FontFamily = "navigation";
            }
            btn.TextColor = Color.White;


            DosisEntry1.Culture = new System.Globalization.CultureInfo("en-US");
            DosisEntry2.Culture = new System.Globalization.CultureInfo("en-US");
            InfusionEntry3.Culture = new System.Globalization.CultureInfo("en-US");
            InfusionEntry4.Culture = new System.Globalization.CultureInfo("en-US");

        }
        private void Button_Clicked(object sender, EventArgs e)
        {
            opcionPicker.IsOpen = !opcionPicker.IsOpen;
        }

        private void lBotonOk_Clicked(object sender, EventArgs e)
        {
            opcionPicker.IsOpen = !opcionPicker.IsOpen;
        }



        void opcionPicker_Changed(object sender, EventArgs e)
        {
            if (opcionPicker.SelectedItem.ToString() == "")
            {
                btnOpcion.Text = "¿Que conversion desea hacer?";

            }
            else
            {
                btnOpcion.Text = opcionPicker.SelectedItem.ToString();
                Opcion = Convert.ToInt32(opcionPicker.SelectedIndex);

                switch (Opcion)
                {
                    //mcgminTOmlhr
                    case 1:
                        mcgminTOmlhr.IsVisible = true;
                        mcgkgminTOmlhr.IsVisible = false;
                        mlhrTOmcgmin.IsVisible = false;
                        mlhrTOmcgkgmin.IsVisible = false;
                        btnAceptar.IsVisible = true;
                        btnAceptar.IsEnabled = true;

                        DosisEntry1.Value = "";
                        lblErrorDosis1.Text = "";
                        CantDroga1.Value = "";
                        lblbErrorCantDroga1.Text = "";
                        Dilucion1.Value = "";
                        lblbErrorDilucion1.Text = "";
                        break;
                    //mcgkgminTOmlhr
                    case 2:
                        mcgkgminTOmlhr.IsVisible = true;
                        mcgminTOmlhr.IsVisible = false;
                        mlhrTOmcgmin.IsVisible = false;
                        mlhrTOmcgkgmin.IsVisible = false;
                        btnAceptar.IsVisible = true;
                        btnAceptar.IsEnabled = true;

                        DosisEntry2.Value = "";
                        lblbErrorDilucion2.Text = "";
                        CantDroga2.Value = "";
                        lblbErrorDilucion2.Text = "";
                        Dilucion2.Value = "";
                        lblbErrorDilucion2.Text = "";
                        PesoEntry2.Value = "";
                        lblErrorPeso2.Text = "";
                        break;
                    //mlhrTOmcgmin
                    case 3:
                        mlhrTOmcgmin.IsVisible = true;
                        mcgkgminTOmlhr.IsVisible = false;
                        mcgminTOmlhr.IsVisible = false;
                        mlhrTOmcgkgmin.IsVisible = false;
                        btnAceptar.IsVisible = true;
                        btnAceptar.IsEnabled = true;

                        InfusionEntry3.Value = "";
                        lblbErrorInfusion3.Text = "";
                        CantDroga3.Value = "";
                        lblbErrorCantDroga3.Text = "";
                        Dilucion3.Value = "";
                        lblbErrorDilucion3.Text = "";

                        break;
                    //mlhrTOmcgkgmin
                    case 4:
                        mlhrTOmcgkgmin.IsVisible = true;
                        mlhrTOmcgmin.IsVisible = false;
                        mcgkgminTOmlhr.IsVisible = false;
                        mcgminTOmlhr.IsVisible = false;
                        btnAceptar.IsVisible = true;
                        btnAceptar.IsEnabled = true;

                        PesoEntry4.Value = "";
                        lblErrorPeso4.Text = "";
                        CantDroga4.Value = "";
                        lblbErrorCantDroga4.Text = "";
                        Dilucion4.Value = "";
                        lblbErrorDilucion4.Text = "";
                        Dilucion4.Value = "";
                        lblbErrorDilucion4.Text = "";
                        break;

                    default:
                        break;
                }



            }

        }




        void Btn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IToggleDrawer>().ToggleDrawer();
        }

        async void BtnAceptar_Clicked(object sender, EventArgs e)
        {

            var Errores = 0;

            //mcgminTOmlhr
            if (Opcion == 1)
            {

                #region Control                         if (DosisEntry1.Value == null)
                {
                    lblErrorDosis1.Text = "La dosis es obligatoria";
                    lblErrorDosis1.TextColor = Color.Red;
                    Errores++;
                }
                if (CantDroga1.Value == null)
                {
                    lblbErrorCantDroga1.Text = "La cantidad de droga es obligatoria";
                    lblbErrorCantDroga1.TextColor = Color.Red;
                    Errores++;
                }

                if (Dilucion1.Value == null)
                {
                    lblbErrorCantDroga1.Text = "El volumen de dilucion es obligatorio";
                    lblbErrorCantDroga1.TextColor = Color.Red;
                    Errores++;
                }

                #endregion 
                #region Procesamiento 
                if (Errores == 0)
                {
                    var lPrimerPaso = ((Convert.ToDouble(DosisEntry1.Value) / 1000) * 60);
                    var lSegundoPaso = (lPrimerPaso / Convert.ToDouble(CantDroga1.Value) * Convert.ToDouble(Dilucion1.Value));
                    await Application.Current.MainPage.DisplayAlert("Resultado", "El equivalente es " + Math.Round(lSegundoPaso, 2).ToString() + " ml/hr", "OK");

                }

                #endregion 

            }

            //mcgKgminTOmlhr
            if (Opcion == 2)
            {

                #region Control

                if (PesoEntry2.Value == null)
                {
                    lblErrorPeso2.Text = "El peso es obligatorio";
                    lblErrorPeso2.TextColor = Color.Red;
                    Errores++;
                }

                if (DosisEntry2.Value == null)
                {
                    lblErrorDosis2.Text = "La dosis es obligatoria";
                    lblErrorDosis2.TextColor = Color.Red;
                    Errores++;
                }
                if (CantDroga2.Value == null)
                {
                    lblbErrorCantDroga2.Text = "La cantidad de droga es obligatoria";
                    lblbErrorCantDroga2.TextColor = Color.Red;
                    Errores++;
                }

                if (Dilucion2.Value == null)
                {
                    lblbErrorCantDroga2.Text = "El volumen de dilucion es obligatorio";
                    lblbErrorCantDroga2.TextColor = Color.Red;
                    Errores++;
                }

                #endregion

                #region Procesamiento

                if (Errores == 0)
                {
                    //1) Obtengo la concentracion en mg en 1 ml en una hora   

                    var lConcentracionMG = ((Convert.ToDouble(DosisEntry2.Value) * Convert.ToDouble(PesoEntry2.Value) / 1000) * 60);

                    //2) Divido la concentraion en 1 ml en una hora por la concentraion de la droga

                    var lConcentracionMgHora = lConcentracionMG / Convert.ToDouble(CantDroga2.Value);

                    //3) Obtengo la cantidad por el volumen final

                    var lMlHora = lConcentracionMgHora * Convert.ToDouble(Dilucion2.Value);


                    await Application.Current.MainPage.DisplayAlert("Resultado", "El equivalente es " + Math.Round(lMlHora, 2).ToString() + " ml/hr", "OK");

                }

                #endregion


            }

            //mlhrTOmcgmin
            if (Opcion == 3)
            {
                #region Control

                if (InfusionEntry3.Value == null)
                {
                    lblbErrorInfusion3.Text = "La infusion es obligatoria";
                    lblbErrorInfusion3.TextColor = Color.Red;
                    Errores++;
                }
                if (CantDroga3.Value == null)
                {
                    lblbErrorCantDroga3.Text = "La cantidad de droga es obligatoria";
                    lblbErrorCantDroga3.TextColor = Color.Red;
                    Errores++;
                }

                if (Dilucion3.Value == null)
                {
                    lblbErrorCantDroga3.Text = "El volumen de dilucion es obligatorio";
                    lblbErrorCantDroga3.TextColor = Color.Red;
                    Errores++;
                }

                #endregion
                #region Procesamiento
                if (Errores == 0)
                {
                    var lPaso1 = (Convert.ToDouble(InfusionEntry3.Value) / Convert.ToDouble(Dilucion3.Value)) * Convert.ToDouble(CantDroga3.Value);
                    var lPaso2 = (lPaso1 / 60) * 1000;

                    await Application.Current.MainPage.DisplayAlert("Resultado", "El equivalente es " + Math.Round(lPaso2, 2).ToString() + " mcg/min", "OK");

                }

                #endregion

            }

            //mlhrTOmcgkgmin
            if (Opcion == 4)
            {
                #region Control

                if (PesoEntry4.Value == null)
                {
                    lblErrorPeso4.Text = "El peso es obligatorio";
                    lblErrorPeso4.TextColor = Color.Red;
                    Errores++;
                }

                if (InfusionEntry4.Value == null)
                {
                    lblbErrorInfusion4.Text = "La infusion es obligatoria";
                    lblbErrorInfusion4.TextColor = Color.Red;
                    Errores++;
                }
                if (CantDroga4.Value == null)
                {
                    lblbErrorCantDroga4.Text = "La cantidad de droga es obligatoria";
                    lblbErrorCantDroga4.TextColor = Color.Red;
                    Errores++;
                }

                if (Dilucion4.Value == null)
                {
                    lblbErrorCantDroga4.Text = "El volumen de dilucion es obligatorio";
                    lblbErrorCantDroga4.TextColor = Color.Red;
                    Errores++;
                }

                #endregion

                #region Procesamiento
                if (Errores == 0)
                {
                    //1) Dosis/Volumen x Cantidad
                    var lPaso1 = (Convert.ToDouble(InfusionEntry4.Value) / Convert.ToDouble(Dilucion4.Value)) * Convert.ToDouble(CantDroga4.Value);
                    //2) De mg/hora a mcg/kg/min
                    var lPaso2 = (lPaso1 / 60 * 1000) / Convert.ToDouble(PesoEntry4.Value);


                    await Application.Current.MainPage.DisplayAlert("Resultado", "El equivalente es " + Math.Round(lPaso2, 2).ToString() + " mcg/kg/min", "OK");

                }
                #endregion
            }
        }



        public class NumericTextBoxRenderer : Syncfusion.SfNumericTextBox.XForms.SfNumericTextBox
        {
            public NumericTextBoxRenderer()
            {

            }
        }
        public class NumericTextBoxRenderer2 : Syncfusion.SfNumericTextBox.XForms.SfNumericTextBox
        {
            public NumericTextBoxRenderer2()
            {

            }
        }

    }
}
