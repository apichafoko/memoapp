﻿using System;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MemoApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUp : PopupPage
    {
        public PopUp(string Mensaje)
        {
            InitializeComponent();
            lblMensaje.Text = Mensaje;

        }

        private async void BotonVolver_OnClicked(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync();
        }
    }

}