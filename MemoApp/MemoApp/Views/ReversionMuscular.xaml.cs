﻿using Syncfusion.SfNavigationDrawer.XForms;
using Syncfusion.XForms.PopupLayout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using MemoApp.Helpers;

namespace MemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReversionMuscular : ContentPage
    {
        SfPopupLayout popupLayout;

        public ReversionMuscular()
        {

            InitializeComponent();
            headerLabel.Text = "Reversion Muscular";
            btn.BackgroundColor = Color.FromHex("#00a0e1");

            btn.HeightRequest = 40;
            btn.WidthRequest = 40;
            btn.Text = "H";
            btn.FontSize = 16;

            popupLayout = new SfPopupLayout();


            if (Device.OS == TargetPlatform.Android)
            {
                if (Device.Idiom == TargetIdiom.Tablet)
                    btn.WidthRequest = 60;
                btn.FontFamily = "navigation.ttf#navigation";
            }
            else
            {
                btn.Image = (FileImageSource)ImageSource.FromFile("hamburgericon.png");
                btn.FontFamily = "navigation";
            }
            btn.TextColor = Color.White;

        }

        void Btn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IToggleDrawer>().ToggleDrawer();
        }


        async void BtnAceptar_Clicked(object sender, EventArgs e)
        {
        }

        async void TipoControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Monitero cuantitativo
            if (TipoControl.SelectedIndex == 0)
            {
                Relajante.Focus();
                Relajante.IsVisible = true;
            }

            //Monitero cualitativo
            if (TipoControl.SelectedIndex == 1)
            {
                Relajante.Focus();
                Relajante.IsVisible = true;
            }

            //Monitero sin monitoreo
            if (TipoControl.SelectedIndex == 2)
            {
                Relajante.IsVisible = false;
                StackCualitativo.IsVisible = false;
                StackCuantitativo.IsVisible = false;

                var lMensaje = "\n \n";
                lMensaje = "1. Deberia considerarse el uso de anticolinesterasicos. La recuperacion espontanea de la funcion neuromuscular puede requerir varias horas en un porcentaje significativo de pacientes, incluso tras una unica dosis de intubacion de un BNM de accion intermedia.";
                lMensaje = lMensaje + "\n \n \n";
                lMensaje = lMensaje + "2. No deberian administrarse anticolinesterasicos hasta observar cierta evidencia de recuperacion de la fuerza muscular, ya que la administracion de anticolinesterasicos con un grado profundo de paralisis puede retrasar la recuperacion neuromuscular.";
                lMensaje = lMensaje + "\n \n \n ";
                lMensaje = lMensaje + "3. Las decisiones en relacion con usar o evitar los anticolinesterasicos no deberian basarse solo en las pruebas clinicas de la fuerza muscular (levantar la cabeza 5s). Muchos pacientes pueden realizar estas pruebas incluso en presencia de un bloqueo neuromuscular profundo (Cociente TOF < 0.5). En el momento en que estos pacientes pueden realizar con exito estas pruebas pueden estar afectados significativamente otros grupos musculares (musculos faringeos).";

                var page = new PopUpCompleto(lMensaje);
                await Navigation.PushPopupAsync(page);

            }
        }

        void Relajante_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Curonio
            if (Relajante.SelectedIndex == 1 || Relajante.SelectedIndex == 2)
            {
                Suga.Focus();
                Suga.IsVisible = true;
            }
            else
            {
                //Atracurio

                //Cuantitativo
                if (TipoControl.SelectedIndex == 0)
                {                    
                    StackCuantitativo.IsVisible = true;
                    StackCualitativo.IsVisible = false;
                    RespuestaTOFSuga.IsVisible = false;
                    RespuestaTOFNeo.IsVisible = true;
                    RespuestaTOFNeo.Focus();

                    Suga.IsVisible = false;
                }

                //Cualitativo
                if (TipoControl.SelectedIndex == 1)
                {
                    StackCualitativo.IsVisible = true;
                    StackCuantitativo.IsVisible = false;
                    RespuestaTOFSugaCuali.IsVisible = false;
                    RespuestaTOFNeoCuali.IsVisible = true;
                    RespuestaTOFNeoCuali.Focus();

                    Suga.IsVisible = false;
                }
            }
        }

        void Suga_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Monitoreo Cuantitativo y Suggamadex
            if (TipoControl.SelectedIndex == 0 && Suga.SelectedIndex == 0)
            {
                StackCuantitativo.IsVisible = true;
                StackCualitativo.IsVisible = false;

                RespuestaTOFSuga.IsVisible = true;
                RespuestaTOFSuga.Focus();
                RespuestaTOFNeo.IsVisible = false;
            }

            // Monitoreo Cuantitativo y Neostigmina
            if (TipoControl.SelectedIndex == 0 && Suga.SelectedIndex == 1)
            {
                StackCuantitativo.IsVisible = true;
                StackCualitativo.IsVisible = false;
                RespuestaTOFSuga.IsVisible = false;
                RespuestaTOFNeo.IsVisible = true;
                RespuestaTOFNeo.Focus();
            }

            // Monitoreo Cualitativo y Suggamadex
            if (TipoControl.SelectedIndex == 1 && Suga.SelectedIndex == 0)
            {
                StackCualitativo.IsVisible = true;
                StackCuantitativo.IsVisible = false;

                RespuestaTOFSugaCuali.IsVisible = true;
                RespuestaTOFSugaCuali.Focus();
                RespuestaTOFNeoCuali.IsVisible = false;
            }

            // Monitoreo Cualitativo y Neostigmina
            if (TipoControl.SelectedIndex == 1 && Suga.SelectedIndex == 1)
            {
                StackCualitativo.IsVisible = true;
                StackCuantitativo.IsVisible = false;
                RespuestaTOFSugaCuali.IsVisible = false;
                RespuestaTOFNeoCuali.IsVisible = true;
                RespuestaTOFNeoCuali.Focus();
            }
        }

        async void RespuestaTOFSuga_SelectedIndexChanged(object sender, EventArgs e)
        {

            RespuestaPTC.IsVisible = false;


            //TOFcnt 0;
            if (RespuestaTOFSuga.SelectedIndex == 0)
            {
                RespuestaPTC.IsVisible = true;
                RespuestaPTC.Focus();
            }
            //TOFcnt 1 - 4;                    
            if (RespuestaTOFSuga.SelectedIndex == 1)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Puedes utilizar " + Settings.Paciente.Peso * 2 + " mg de Suggamadex");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);

            }
            //TOFRatio < 90%

            if (RespuestaTOFSuga.SelectedIndex == 2)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Puedes utilizar " + Settings.Paciente.Peso * 2 + " mg de Suggamadex");
                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }
            //TOFRatio > 90%
            if (RespuestaTOFSuga.SelectedIndex == 3)
            {
                var MensajeShow = new PopUp("No se recomienda la reversión en este caso");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };
                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }

        }

        async void RespuestaTOFNeo_SelectedIndexChanged(object sender, EventArgs e)
        {


            //TOFcnt 0 - 1;
            if (RespuestaTOFNeo.SelectedIndex == 0)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Se recomienda el retraso de la reversion hasta un recuento de TOF de 2");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }
            //TOFcnt 2 - 3;                    
            if (RespuestaTOFNeo.SelectedIndex == 1)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Podes utilizar " + (0.05 * Settings.Paciente.Peso) + " mg de Neostigmina acompañado de " + (0.01 * Settings.Paciente.Peso) + " mg de atropina");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);

            }
            //TOFRatio < 40%

            if (RespuestaTOFNeo.SelectedIndex == 2)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Podes utilizar " + (0.05 * Settings.Paciente.Peso) + " mg de Neostigmina acompañado de " + (0.01 * Settings.Paciente.Peso) + " mg de atropina");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }
            //TOFRatio 40 - 90%
            if (RespuestaTOFNeo.SelectedIndex == 3)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Podes utilizar " + (0.02 * Settings.Paciente.Peso) + " mg de Neostigmina acompañado de " + (0.01 * Settings.Paciente.Peso) + " mg de atropina");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }

            //TOFRatio > 90%
            if (RespuestaTOFNeo.SelectedIndex == 4)
            {
                var MensajeShow = new PopUp("No se recomienda la reversión en este caso");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };
                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }

        }


        async void RespuestaTOFSugaCuali_SelectedIndexChanged(object sender, EventArgs e)
        {

            //TOFcnt 0;
            if (RespuestaTOFSugaCuali.SelectedIndex == 0)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Puedes utilizar " + Settings.Paciente.Peso * 16 + " mg de Suggamadex");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }
            //TOFcnt 1 - 4;                    
            if (RespuestaTOFSugaCuali.SelectedIndex == 1)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Puedes utilizar " + Settings.Paciente.Peso * 2 + " mg de Suggamadex");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);

            }


        }

        async void RespuestaTOFNeoCuali_SelectedIndexChanged(object sender, EventArgs e)
        {


            //TOFcnt 0 - 1;
            if (RespuestaTOFNeoCuali.SelectedIndex == 0)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Se recomienda el retraso de la reversion hasta un recuento de TOF de 2");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }
            //TOFcnt 2 - 3;                    
            if (RespuestaTOFNeoCuali.SelectedIndex == 1)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Podes utilizar " + (0.05 * Settings.Paciente.Peso) + " mg de Neostigmina acompañado de " + (0.01 * Settings.Paciente.Peso) + " mg de atropina");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);

            }
            //TOFcnt de 4;                    

            if (RespuestaTOFNeoCuali.SelectedIndex == 2)
            {
                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp("Podes utilizar " + (0.04 * Settings.Paciente.Peso) + " mg de Neostigmina acompañado de " + (0.01 * Settings.Paciente.Peso) + " mg de atropina");

                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }


        }



        async void RespuestaPTCSuga_SelectedIndexChanged(object sender, EventArgs e)
        {
            var Mensaje = "";

            //PTC = 0
            if (RespuestaPTC.SelectedIndex == 0)
            {
                Mensaje = "Puedes utilizar " + Settings.Paciente.Peso * 16 + " mg de Suggamadex";

                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp(Mensaje);
                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }

            //PTC 1 a 15

            if (RespuestaPTC.SelectedIndex == 1)
            {
                Mensaje = "Puedes utilizar " + Settings.Paciente.Peso * 4 + " mg de Suggamadex";

                //Instancio el popup por si lo voy a usar
                var MensajeShow = new PopUp(Mensaje);
                MensajeShow.Animation = new ScaleAnimation
                {
                    DurationOut = 300,
                    DurationIn = 300,
                    EasingIn = Easing.SinIn,
                    EasingOut = Easing.SpringIn
                };

                MensajeShow.BackgroundColor = Color.FromRgba(0, 0, 0, 0.75);
                MensajeShow.CloseWhenBackgroundIsClicked = true;

                await Navigation.PushPopupAsync(MensajeShow);
            }
        }



    }

}