﻿using Syncfusion.SfNavigationDrawer.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MemoApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Dosis : ContentPage
	{
		public Dosis ()
		{
            InitializeComponent();

            headerLabel.Text = "Dosis";
            btn.BackgroundColor = Color.FromHex("#00a0e1");

            btn.HeightRequest = 40;
            btn.WidthRequest = 40;
            btn.Text = "H";
            btn.FontSize = 16;

            if (Device.OS == TargetPlatform.Android)
            {
                if (Device.Idiom == TargetIdiom.Tablet)
                    btn.WidthRequest = 60;
                btn.FontFamily = "navigation.ttf#navigation";
            }
            else
            {
                btn.Image = (FileImageSource)ImageSource.FromFile("hamburgericon.png");
                btn.FontFamily = "navigation";
            }
            btn.TextColor = Color.White;
        }

        void Btn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IToggleDrawer>().ToggleDrawer();
        }
    }
}