﻿using MemoApp.Helpers;
using MemoApp.SignosHelpers;
using Rg.Plugins.Popup.Extensions;
using Syncfusion.SfNavigationDrawer.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignosVitales : ContentPage
    {
        public SignosVitales()
        {
            InitializeComponent();
            headerLabel.Text = "Param. Vitales";
            btn.BackgroundColor = Color.FromHex("#00a0e1");

            btn.HeightRequest = 40;
            btn.WidthRequest = 40;
            btn.Text = "H";
            btn.FontSize = 16;


            if (Device.OS == TargetPlatform.Android)
            {
                if (Device.Idiom == TargetIdiom.Tablet)
                    btn.WidthRequest = 60;
                btn.FontFamily = "navigation.ttf#navigation";
            }
            else
            {
                btn.Image = (FileImageSource)ImageSource.FromFile("hamburgericon.png");
                btn.FontFamily = "navigation";
            }
            btn.TextColor = Color.White;

            SignosVitalesRepository SignosRepository = new SignosVitalesRepository();

            #region Cardio
            var lSignosCardiacos = SignosRepository.GetSignos().Where(x => x.Categoria == "Cardiaco");

            var GrillaCardio = new Grid();
            var RowCardioDefinitions = new RowDefinitionCollection();


            for (int i = 0; i < lSignosCardiacos.Count(); i++)
            {
                RowCardioDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            }
          
            GrillaCardio.RowDefinitions = RowCardioDefinitions;
            var filaCardio = 0;

            foreach (var lSignoCardiaco in lSignosCardiacos)
            {
                var lLabelNombre = new Label() { FontAttributes = FontAttributes.Bold, FontSize = 18, TextColor = Color.FromHex("#474747"), Opacity = 0.67, Text = lSignoCardiaco.Nombre + ": ", HorizontalOptions = LayoutOptions.Start };
                lLabelNombre.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() => OnLabelClicked("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus in erat eget mauris semper vehicula. Quisque at fringilla felis, id faucibus ligula. Duis nisl felis, faucibus et felis nec, fermentum lobortis ligula. Pellentesque vulputate bibendum dapibus. Integer vitae sollicitudin urna. Etiam auctor cursus enim ut fermentum. In posuere commodo dui quis finibus. Maecenas neque quam, malesuada id sapien in, posuere consequat sapien. Sed eros arcu, tincidunt sit amet neque consectetur, volutpat varius velit. In tempus nec quam pulvinar consectetur. In feugiat non elit eu efficitur."))
                });

                var lLabelDescripcion = new Label() { FontAttributes = FontAttributes.Bold, FontSize = 18, TextColor = Color.FromHex("#1338FF"), Text = lSignoCardiaco.Descripcion, HorizontalOptions = LayoutOptions.Start };
                lLabelNombre.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    //Command = new Command(() => OnLabelClicked("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus in erat eget mauris semper vehicula. Quisque at fringilla felis, id faucibus ligula. Duis nisl felis, faucibus et felis nec, fermentum lobortis ligula. Pellentesque vulputate bibendum dapibus. Integer vitae sollicitudin urna. Etiam auctor cursus enim ut fermentum. In posuere commodo dui quis finibus. Maecenas neque quam, malesuada id sapien in, posuere consequat sapien. Sed eros arcu, tincidunt sit amet neque consectetur, volutpat varius velit. In tempus nec quam pulvinar consectetur. In feugiat non elit eu efficitur."))
                });


                GrillaCardio.Children.Add(lLabelNombre, 0, filaCardio);
                GrillaCardio.Children.Add(lLabelDescripcion, 1, filaCardio);
                filaCardio++;
            }

            stackContenidoCardiaco.Children.Add(GrillaCardio);
            #endregion

            #region Respi
            var lSignosRespiratorios = SignosRepository.GetSignos().Where(x => x.Categoria == "Respiratorio");

            var GrillaRespi = new Grid();
            var RowRespiDefinitions = new RowDefinitionCollection();


            for (int i = 0; i < lSignosRespiratorios.Count(); i++)
            {
                RowRespiDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            }

            var filaRespi = 0;

            GrillaRespi.RowDefinitions = RowRespiDefinitions;

            foreach (var lSignoRespi in lSignosRespiratorios)
            {

                GrillaRespi.Children.Add(new Label() { FontAttributes = FontAttributes.Bold, FontSize = 18, TextColor = Color.FromHex("#474747"), Opacity = 0.67, Text = lSignoRespi.Nombre + ": " }, 0, filaRespi);
                GrillaRespi.Children.Add(new Label() { FontAttributes = FontAttributes.Bold, FontSize = 18, TextColor = Color.FromHex("#1338FF"), Text = lSignoRespi.Descripcion }, 1, filaRespi);
                filaRespi++;
            }

            stackContenidoRespiratorio.Children.Add(GrillaRespi);
            #endregion


            #region Varios
            var lSignosVarios = SignosRepository.GetSignos().Where(x => x.Categoria == "Varios");

            var GrillaVarios = new Grid();
            var RowVariosDefinitions = new RowDefinitionCollection();


            for (int i = 0; i < lSignosVarios.Count(); i++)
            {
                RowVariosDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            }
           
            GrillaVarios.RowDefinitions = RowVariosDefinitions;
            
            var filaVarios = 0;

            foreach (var lSignoVario in lSignosVarios)
            {

                GrillaVarios.Children.Add(new Label() { FontAttributes = FontAttributes.Bold, FontSize = 18, TextColor = Color.FromHex("#474747"), Opacity = 0.67, Text = lSignoVario.Nombre + ": " }, 0, filaVarios);
                GrillaVarios.Children.Add(new Label() { FontAttributes = FontAttributes.Bold, FontSize = 18, TextColor = Color.FromHex("#1338FF"), Text = lSignoVario.Descripcion }, 1, filaVarios);
                filaVarios++;
            }

            stackContenidoVarios.Children.Add(GrillaVarios);
            #endregion
        }


        void Btn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IToggleDrawer>().ToggleDrawer();
        }

        async void OnLabelClicked(string Numero)
        {
            
            var page = new PopUpCompleto(Numero);
            await Navigation.PushPopupAsync(page);
            
        }
    }
}