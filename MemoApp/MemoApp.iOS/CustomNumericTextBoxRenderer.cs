﻿using System;
using MemoApp.iOS;
using Syncfusion.SfNumericTextBox.XForms.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using static MemoApp.Views.Calculadora;

[assembly: ExportRenderer(typeof(NumericTextBoxRenderer2), typeof(CustomNumericTextBoxRenderer))]
namespace MemoApp.iOS
{
    public class CustomNumericTextBoxRenderer : SfNumericTextBoxRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Syncfusion.SfNumericTextBox.XForms.SfNumericTextBox> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Layer.CornerRadius = 5;
                Control.Layer.BorderWidth = 1;
                Control.ClipsToBounds = true;
            }
        }
    }
}