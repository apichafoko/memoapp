package md500f619f6249de12d9ccfbfb198aaf2ca;


public class CustomNumericBoxRenderer
	extends md5e06cae45bf5ee0365f6266773cdabf54.SfNumericTextBoxRenderer
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("MemoApp.Droid.CustomNumericBoxRenderer, MemoApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CustomNumericBoxRenderer.class, __md_methods);
	}


	public CustomNumericBoxRenderer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == CustomNumericBoxRenderer.class)
			mono.android.TypeManager.Activate ("MemoApp.Droid.CustomNumericBoxRenderer, MemoApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public CustomNumericBoxRenderer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == CustomNumericBoxRenderer.class)
			mono.android.TypeManager.Activate ("MemoApp.Droid.CustomNumericBoxRenderer, MemoApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public CustomNumericBoxRenderer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == CustomNumericBoxRenderer.class)
			mono.android.TypeManager.Activate ("MemoApp.Droid.CustomNumericBoxRenderer, MemoApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
