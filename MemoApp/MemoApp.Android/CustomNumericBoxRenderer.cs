﻿using System;
using MemoApp.Droid;
using Syncfusion.SfNumericTextBox.XForms.Droid;
using Xamarin.Forms;
using static MemoApp.Views.Calculadora;

[assembly: ExportRenderer(typeof(NumericTextBoxRenderer), typeof(CustomNumericBoxRenderer))]
namespace MemoApp.Droid
{
    public class CustomNumericBoxRenderer : SfNumericTextBoxRenderer
    {
        public CustomNumericBoxRenderer()
        {
        }

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Syncfusion.SfNumericTextBox.XForms.SfNumericTextBox> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                Control.SetBackgroundResource(0);
            }
        }
    }
}